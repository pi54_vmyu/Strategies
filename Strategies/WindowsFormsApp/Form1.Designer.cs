﻿namespace WindowsFormsApp
{
    partial class Strategy
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Strategy));
            this.LabelCount1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.numericStrCount1 = new System.Windows.Forms.NumericUpDown();
            this.numericStrCount2 = new System.Windows.Forms.NumericUpDown();
            this.buttonGenerate = new System.Windows.Forms.Button();
            this.dataGridViewArray1 = new System.Windows.Forms.DataGridView();
            this.buttonShowAnswer = new System.Windows.Forms.Button();
            this.buttonUserSolve = new System.Windows.Forms.Button();
            this.buttonStep = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.режимToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findStrategies = new System.Windows.Forms.ToolStripMenuItem();
            this.findFunctions = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonNoAnswer = new System.Windows.Forms.Button();
            this.buttonShowFunctions = new System.Windows.Forms.Button();
            this.solveFunctions = new System.Windows.Forms.Button();
            this.labelColor = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericStrCount1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericStrCount2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelCount1
            // 
            this.LabelCount1.AutoSize = true;
            this.LabelCount1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.LabelCount1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LabelCount1.Location = new System.Drawing.Point(12, 45);
            this.LabelCount1.Name = "LabelCount1";
            this.LabelCount1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.LabelCount1.Size = new System.Drawing.Size(288, 31);
            this.LabelCount1.TabIndex = 1;
            this.LabelCount1.Text = "Кількість стратегій гравця №1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label1.Location = new System.Drawing.Point(12, 88);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.label1.Size = new System.Drawing.Size(288, 31);
            this.label1.TabIndex = 3;
            this.label1.Text = "Кількість стратегій гравця №2";
            // 
            // numericStrCount1
            // 
            this.numericStrCount1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.numericStrCount1.Location = new System.Drawing.Point(322, 45);
            this.numericStrCount1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericStrCount1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericStrCount1.Name = "numericStrCount1";
            this.numericStrCount1.Size = new System.Drawing.Size(120, 31);
            this.numericStrCount1.TabIndex = 4;
            this.numericStrCount1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericStrCount2
            // 
            this.numericStrCount2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.numericStrCount2.Location = new System.Drawing.Point(322, 88);
            this.numericStrCount2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericStrCount2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericStrCount2.Name = "numericStrCount2";
            this.numericStrCount2.Size = new System.Drawing.Size(120, 31);
            this.numericStrCount2.TabIndex = 4;
            this.numericStrCount2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonGenerate
            // 
            this.buttonGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGenerate.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonGenerate.Location = new System.Drawing.Point(478, 106);
            this.buttonGenerate.Name = "buttonGenerate";
            this.buttonGenerate.Size = new System.Drawing.Size(130, 41);
            this.buttonGenerate.TabIndex = 6;
            this.buttonGenerate.Text = "Генерувати";
            this.buttonGenerate.UseVisualStyleBackColor = false;
            this.buttonGenerate.Click += new System.EventHandler(this.buttonGenerate_Click);
            // 
            // dataGridViewArray1
            // 
            this.dataGridViewArray1.AllowUserToAddRows = false;
            this.dataGridViewArray1.AllowUserToDeleteRows = false;
            this.dataGridViewArray1.AllowUserToResizeColumns = false;
            this.dataGridViewArray1.AllowUserToResizeRows = false;
            this.dataGridViewArray1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewArray1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewArray1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridViewArray1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArray1.ColumnHeadersVisible = false;
            this.dataGridViewArray1.Enabled = false;
            this.dataGridViewArray1.Location = new System.Drawing.Point(346, 175);
            this.dataGridViewArray1.Name = "dataGridViewArray1";
            this.dataGridViewArray1.ReadOnly = true;
            this.dataGridViewArray1.RowHeadersVisible = false;
            this.dataGridViewArray1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridViewArray1.Size = new System.Drawing.Size(98, 188);
            this.dataGridViewArray1.TabIndex = 7;
            this.dataGridViewArray1.Visible = false;
            this.dataGridViewArray1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewArray1_CellDoubleClick);
            // 
            // buttonShowAnswer
            // 
            this.buttonShowAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowAnswer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonShowAnswer.Enabled = false;
            this.buttonShowAnswer.Location = new System.Drawing.Point(638, 35);
            this.buttonShowAnswer.Name = "buttonShowAnswer";
            this.buttonShowAnswer.Size = new System.Drawing.Size(130, 60);
            this.buttonShowAnswer.TabIndex = 8;
            this.buttonShowAnswer.Text = "Показати результат";
            this.buttonShowAnswer.UseVisualStyleBackColor = false;
            this.buttonShowAnswer.Click += new System.EventHandler(this.buttonShowAnswer_Click);
            // 
            // buttonUserSolve
            // 
            this.buttonUserSolve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonUserSolve.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonUserSolve.Enabled = false;
            this.buttonUserSolve.Location = new System.Drawing.Point(478, 35);
            this.buttonUserSolve.Name = "buttonUserSolve";
            this.buttonUserSolve.Size = new System.Drawing.Size(130, 60);
            this.buttonUserSolve.TabIndex = 9;
            this.buttonUserSolve.Text = "Розв\'язувати самостійно";
            this.buttonUserSolve.UseVisualStyleBackColor = false;
            this.buttonUserSolve.Click += new System.EventHandler(this.buttonUserSolve_Click);
            // 
            // buttonStep
            // 
            this.buttonStep.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonStep.Enabled = false;
            this.buttonStep.Location = new System.Drawing.Point(638, 106);
            this.buttonStep.Name = "buttonStep";
            this.buttonStep.Size = new System.Drawing.Size(130, 41);
            this.buttonStep.TabIndex = 10;
            this.buttonStep.Text = "Крок";
            this.buttonStep.UseVisualStyleBackColor = false;
            this.buttonStep.Click += new System.EventHandler(this.buttonStep_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.menuStrip1.Font = new System.Drawing.Font("Book Antiqua", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.режимToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(784, 29);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // режимToolStripMenuItem
            // 
            this.режимToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findStrategies,
            this.findFunctions});
            this.режимToolStripMenuItem.Name = "режимToolStripMenuItem";
            this.режимToolStripMenuItem.Size = new System.Drawing.Size(81, 27);
            this.режимToolStripMenuItem.Text = "Режим";
            // 
            // findStrategies
            // 
            this.findStrategies.Name = "findStrategies";
            this.findStrategies.Size = new System.Drawing.Size(336, 28);
            this.findStrategies.Text = "Пошук виграшних стратегій";
            this.findStrategies.Click += new System.EventHandler(this.findStrategies_Click);
            // 
            // findFunctions
            // 
            this.findFunctions.Name = "findFunctions";
            this.findFunctions.Size = new System.Drawing.Size(336, 28);
            this.findFunctions.Text = "Побудова функцій реакції";
            this.findFunctions.Click += new System.EventHandler(this.findFunctions_Click);
            // 
            // buttonNoAnswer
            // 
            this.buttonNoAnswer.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonNoAnswer.Location = new System.Drawing.Point(705, 451);
            this.buttonNoAnswer.Name = "buttonNoAnswer";
            this.buttonNoAnswer.Size = new System.Drawing.Size(75, 30);
            this.buttonNoAnswer.TabIndex = 12;
            this.buttonNoAnswer.Text = "Немає";
            this.buttonNoAnswer.UseVisualStyleBackColor = false;
            this.buttonNoAnswer.Visible = false;
            // 
            // buttonShowFunctions
            // 
            this.buttonShowFunctions.BackColor = System.Drawing.SystemColors.ControlDark;
            this.buttonShowFunctions.Location = new System.Drawing.Point(478, 35);
            this.buttonShowFunctions.Name = "buttonShowFunctions";
            this.buttonShowFunctions.Size = new System.Drawing.Size(130, 60);
            this.buttonShowFunctions.TabIndex = 13;
            this.buttonShowFunctions.Text = "Побудувати функції";
            this.buttonShowFunctions.UseVisualStyleBackColor = false;
            this.buttonShowFunctions.Visible = false;
            this.buttonShowFunctions.Click += new System.EventHandler(this.buttonShowFunctions_Click);
            // 
            // solveFunctions
            // 
            this.solveFunctions.BackColor = System.Drawing.SystemColors.ControlDark;
            this.solveFunctions.Location = new System.Drawing.Point(638, 35);
            this.solveFunctions.Name = "solveFunctions";
            this.solveFunctions.Size = new System.Drawing.Size(130, 60);
            this.solveFunctions.TabIndex = 14;
            this.solveFunctions.Text = "Знайти функції реакції";
            this.solveFunctions.UseVisualStyleBackColor = false;
            this.solveFunctions.Visible = false;
            this.solveFunctions.Click += new System.EventHandler(this.solveFunctions_Click);
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.BackColor = System.Drawing.SystemColors.ControlDark;
            this.labelColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelColor.Font = new System.Drawing.Font("Book Antiqua", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelColor.ForeColor = System.Drawing.Color.Black;
            this.labelColor.Location = new System.Drawing.Point(638, 36);
            this.labelColor.Name = "labelColor";
            this.labelColor.Padding = new System.Windows.Forms.Padding(3, 3, 3, 3);
            this.labelColor.Size = new System.Drawing.Size(110, 110);
            this.labelColor.TabIndex = 1;
            this.labelColor.Text = "Гравець 1 \r\n(LightBlue)\r\nГравець 2 \r\n(LightPink)\r\nСпільна \r\n(LightSeaGreen)";
            this.labelColor.Visible = false;
            // 
            // Strategy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 23F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 482);
            this.Controls.Add(this.solveFunctions);
            this.Controls.Add(this.buttonShowFunctions);
            this.Controls.Add(this.buttonNoAnswer);
            this.Controls.Add(this.numericStrCount1);
            this.Controls.Add(this.numericStrCount2);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.LabelCount1);
            this.Controls.Add(this.buttonGenerate);
            this.Controls.Add(this.dataGridViewArray1);
            this.Controls.Add(this.buttonShowAnswer);
            this.Controls.Add(this.buttonStep);
            this.Controls.Add(this.buttonUserSolve);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Book Antiqua", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.MaximumSize = new System.Drawing.Size(804, 525);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "Strategy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Strategies";
            ((System.ComponentModel.ISupportInitialize)(this.numericStrCount1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericStrCount2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArray1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelCount1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericStrCount1;
        private System.Windows.Forms.NumericUpDown numericStrCount2;
        private System.Windows.Forms.Button buttonGenerate;
        private System.Windows.Forms.DataGridView dataGridViewArray1;
        private System.Windows.Forms.Button buttonShowAnswer;
        private System.Windows.Forms.Button buttonUserSolve;
        private System.Windows.Forms.Button step;
        private System.Windows.Forms.Button buttonStep;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem режимToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findStrategies;
        private System.Windows.Forms.ToolStripMenuItem findFunctions;
        private System.Windows.Forms.Button buttonNoAnswer;
        private System.Windows.Forms.Button buttonShowFunctions;
        private System.Windows.Forms.Button solveFunctions;
        private System.Windows.Forms.Label labelColor;
    }
}


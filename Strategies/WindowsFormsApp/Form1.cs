﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class Strategy : Form
    {
        int[,] player1;
        int[,] player2;

        int[,] tmpPlayer1;
        int[,] tmpPlayer2;

        public Strategy()
        {
            InitializeComponent();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
           // solveFunctions.Enabled = true;
            buttonShowFunctions.Enabled = true;
            buttonUserSolve.Enabled = true;
            buttonShowAnswer.Enabled = true;
            buttonStep.Enabled = true;
            int n = Convert.ToInt32(numericStrCount1.Value);
            int m = Convert.ToInt32(numericStrCount2.Value);

            player1 = new int[n, m];
            player2 = new int[n, m];

            dataGridViewArray1.RowCount = n + 1;
            dataGridViewArray1.ColumnCount = m + 1;

            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                dataGridViewArray1.Rows[i + 1].Cells[0].Value = Convert.ToChar(65 + i);
                for (int j = 0; j < m; j++)
                {
                    if (i == 0)
                    {
                        dataGridViewArray1.Rows[0].Cells[j + 1].Value = Convert.ToChar(65 + j);
                    }
                    player1[i, j] = rnd.Next(-9, 10);
                    player2[i, j] = rnd.Next(-9, 10);
                    dataGridViewArray1.Rows[i + 1].Cells[j + 1].Value = Convert.ToString(player1[i, j]) + " ; " + Convert.ToString(player2[i, j]);
                    dataGridViewArray1.Rows[i + 1].Cells[j + 1].Style.BackColor = System.Drawing.Color.White;
                }
            }
            setDataGridSize();
            tmpPlayer1 = player1.Clone() as int[,];
            tmpPlayer2 = player2.Clone() as int[,];
        }

        private void buttonStep_Click(object sender, EventArgs e)
        {
            solvingStep(0);
        }

        private void buttonShowAnswer_Click(object sender, EventArgs e)
        {
            solvingStep(1);
        }

        private void solvingStep(int step)
        {
            bool flag = true;
            bool end = true;
            while (!(player1.GetLength(0) == 1 && player2.GetLength(1) == 1) && flag != false && step != -1)
            {

                flag = doStep();
                if (flag)
                {
                    displayDataGridView();
                }
                else
                {
                    MessageBox.Show("Немає наступного кроку");
                    flag = false;
                    end = false;
                }

                if (step == 0)
                    step = -1;
            }
            setDataGridSize();
            if (end == true && player1.GetLength(0) == 1 && player2.GetLength(1) == 1)
                MessageBox.Show(player1[0, 0].ToString() + " ; " + player2[0, 0].ToString());
        }

        private void buttonUserSolve_Click(object sender, EventArgs e)
        {
            dataGridViewArray1.Enabled = true;
            buttonNoAnswer.Visible = true;
        }

        private bool doStep()
        {
            int[,] matrix = player1;
            for (int j = 0; j < matrix.GetLength(1); j++)
            {

                for (int m = 0; m < matrix.GetLength(1); m++)
                {
                    bool flag = true;
                    if (m == j)
                        continue;
                    for (int i = 0; i < matrix.GetLength(0); i++)
                    {
                        if (matrix[i, j] >= matrix[i, m])
                            flag = false;
                    }
                    if (flag)
                    {
                        player1 = deleteCol(j, matrix);
                        player2 = deleteCol(j, player2);
                        deleteLetter(j + 1, false, true);
                        return true;
                    }
                }
            }
            matrix = player2;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int k = 0; k < matrix.GetLength(0); k++)
                {

                    bool flag = true;
                    if (k == i)
                        continue;
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        if (matrix[i, j] >= matrix[k, j])
                            flag = false;
                    }
                    if (flag)
                    {
                        player2 = deleteRow(i, matrix);
                        player1 = deleteRow(i, player1);
                        deleteLetter(i + 1, true, false);
                        return true;
                    }
                }

            }
            return false;
        }

        private int[,] deleteCol(int index, int[,] player)
        {
            if (player.GetLength(1) <= index)
                return player;

            int tmp = 0;
            int[,] tmpArr = new int[player.GetLength(0), player.GetLength(1) - 1];

            for (int i = 0; i < player.GetLength(0); i++)
            {
                tmp = 0;
                for (int j = 0; j < player.GetLength(1) - 1; j++)
                {
                    if (j == index)
                        tmp = 1;
                    tmpArr[i, j] = player[i, j + tmp];
                }
            }

            return tmpArr;

        }

        private int[,] deleteRow(int index, int[,] player)
        {
            if (player.GetLength(0) <= index)
                return player;
            int[,] tmpArr = new int[player.GetLength(0) - 1, player.GetLength(1)];
            int tmp = 0;

            for (int i = 0; i < player.GetLength(1); i++)
            {
                tmp = 0;
                for (int j = 0; j < player.GetLength(0) - 1; j++)
                {
                    if (j == index)
                        tmp = 1;
                    tmpArr[j, i] = player[j + tmp, i];
                }
            }

            return tmpArr;
        }

        private void setDataGridSize()
        {
            dataGridViewArray1.Width = (player1.GetLength(1) + 1) * 70;
            dataGridViewArray1.Height = (player1.GetLength(0) + 1) * 27;
            dataGridViewArray1.Left = 392 - dataGridViewArray1.Width / 2;
            dataGridViewArray1.Top = 300 - dataGridViewArray1.Height / 2;
            dataGridViewArray1.Visible = true;
        }

        private void buttonNoAnswer_Click(object sender, EventArgs e)
        {
            if (doStep() == false)
            {
                MessageBox.Show("Вірно");
                dataGridViewArray1.Enabled = false;
                return;
            }
            else
            {
                refreshDataGridArray();
                MessageBox.Show("Не вийшло");
                dataGridViewArray1.Enabled = false;
            }
            buttonNoAnswer.Visible = false;

        }

        private void dataGridViewArray1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0 && e.RowIndex == 0) || (e.RowIndex > 0 && e.ColumnIndex > 0))
                return;
            int[,] tmp = player1.Clone() as int[,];
            doStep();
            bool flagRow = cmpArrays(deleteRow(e.RowIndex - 1, tmp), player1);
            bool flagCol = cmpArrays(deleteCol(e.ColumnIndex - 1, tmp), player1);


            if ((e.ColumnIndex == 0 && flagRow) || (e.RowIndex == 0 && flagCol))
            {
                displayDataGridView();
                if (player1.GetLength(0) == 1 && player1.GetLength(1) == 1)
                {
                    MessageBox.Show("Вийшло");
                    buttonNoAnswer.Visible = false;
                    dataGridViewArray1.Enabled = false;
                }
                return;
            }

            else
            {
                refreshDataGridArray();
                MessageBox.Show("Не вийшло");
                buttonNoAnswer.Visible = false;
                dataGridViewArray1.Enabled = false;
            }

        }

        static bool cmpArrays(int[,] a, int[,] b)
        {
            return a.Cast<int>().SequenceEqual(b.Cast<int>());
        }

        private void displayDataGridView()
        {
            dataGridViewArray1.RowCount = player1.GetLength(0) + 1;
            dataGridViewArray1.ColumnCount = player1.GetLength(1) + 1;
            for (int i = 0; i < player1.GetLength(0); i++)
                for (int j = 0; j < player1.GetLength(1); j++)
                    dataGridViewArray1.Rows[i + 1].Cells[j + 1].Value = Convert.ToString(player1[i, j]) + " ; " + Convert.ToString(player2[i, j]);
            setDataGridSize();
        }

        private void deleteLetter(int index, bool row, bool col)
        {
            int tmp = 0;
            if (col)
                for (int i = 0; i < dataGridViewArray1.ColumnCount - 1; i++)
                {
                    if (i == index)
                        tmp = 1;
                    dataGridViewArray1.Rows[0].Cells[i].Value = dataGridViewArray1.Rows[0].Cells[i + tmp].Value;
                }
            if (row)
                for (int i = 0; i < dataGridViewArray1.RowCount - 1; i++)
                {
                    if (i == index)
                        tmp = 1;
                    dataGridViewArray1.Rows[i].Cells[0].Value = dataGridViewArray1.Rows[i + tmp].Cells[0].Value;
                }
            dataGridViewArray1.Rows[0].Cells[0].Value = "";
        }

        private void refreshDataGridArray()
        {
            player1 = tmpPlayer1.Clone() as int[,];
            player2 = tmpPlayer2.Clone() as int[,];

            dataGridViewArray1.RowCount = player1.GetLength(0) + 1;
            dataGridViewArray1.ColumnCount = player1.GetLength(1) + 1;
            for (int i = 0; i < tmpPlayer1.GetLength(0); i++)
            {
                dataGridViewArray1.Rows[i + 1].Cells[0].Value = Convert.ToChar(65 + i);
                for (int j = 0; j < tmpPlayer1.GetLength(1); j++)
                {
                    if (i == 0)
                    {
                        dataGridViewArray1.Rows[0].Cells[j + 1].Value = Convert.ToChar(65 + j);
                    }
                    dataGridViewArray1.Rows[i + 1].Cells[j + 1].Value = Convert.ToString(player1[i, j]) + " ; " + Convert.ToString(player2[i, j]);
                }
            }
            setDataGridSize();
        }

        private void findStrategies_Click(object sender, EventArgs e)
        {

            buttonShowAnswer.Visible = true;
            buttonStep.Visible = true;
            buttonUserSolve.Visible = true;
            buttonStep.Enabled = false;
            buttonUserSolve.Enabled = false;
            dataGridViewArray1.Visible = false;
            // solveFunctions.Visible = false;
            labelColor.Visible = false;
            buttonShowFunctions.Visible = false;
            buttonShowAnswer.Enabled = false;
        }

        private void findFunctions_Click(object sender, EventArgs e)
        {
            //solveFunctions.Visible = true;
            labelColor.Visible = true;
            buttonShowFunctions.Visible = true;
            //solveFunctions.Enabled = false;
            buttonShowFunctions.Enabled = false;
            buttonShowAnswer.Visible = false;
            buttonStep.Visible = false;
            buttonUserSolve.Visible = false;
            dataGridViewArray1.Visible = false;

        }

        private void buttonShowFunctions_Click(object sender, EventArgs e)
        {
            findFunction();
        }

        private void solveFunctions_Click(object sender, EventArgs e)
        {

        }

        private void findFunction()
        {
            int max = -10;
            for (int i = 0; i < player1.GetLength(1); i++)
            {
                max = -10;
                for (int j = 0; j < player1.GetLength(0); j++)
                    if (max < player1[j, i])
                        max = player1[j, i];
                for (int j = 0; j < player1.GetLength(0); j++)
                    if (max == player1[j, i])
                        dataGridViewArray1.Rows[j + 1].Cells[i + 1].Style.BackColor = System.Drawing.Color.LightBlue;

            }

            for (int i = 0; i < player2.GetLength(0); i++)
            {
                max = -10;
                for (int j = 0; j < player2.GetLength(1); j++)
                    if (max < player2[i, j])
                        max = player2[i, j];
                for (int j = 0; j < player2.GetLength(1); j++)
                {
                    if (max == player2[i, j] && dataGridViewArray1.Rows[i + 1].Cells[j + 1].Style.BackColor != System.Drawing.Color.White)
                        dataGridViewArray1.Rows[i + 1].Cells[j + 1].Style.BackColor = System.Drawing.Color.LightSeaGreen;

                    if (max == player2[i, j] && dataGridViewArray1.Rows[i + 1].Cells[j + 1].Style.BackColor == System.Drawing.Color.White)
                        dataGridViewArray1.Rows[i + 1].Cells[j + 1].Style.BackColor = System.Drawing.Color.LightPink;

                }

            }


        }

    }
}
